FROM python:3-alpine
ENV user admin
ENV email admin@example.com
ENV password pass


WORKDIR /app

RUN mkdir db
COPY  requirements.txt /app
RUN pip install -r requirements.txt
COPY . . 


EXPOSE 8000
VOLUME ["/app/db"]

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000


